package pruebas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Cola;
import model.data_structures.Lista;

public class ColaTest 
{
	private Cola<String> cola;

	@Before
	public void setUpEscenario1() {
		cola = new Cola<>();
	}

	public void setUpEscenario2() {
		cola = new Cola<String>();
		cola.enqueue("hola");
	}

	public void setUpEscenario3() {
		cola = new Cola<String>();
		cola.enqueue("hola");
		cola.enqueue("como");
		cola.enqueue("estas");
	}

	@Test
	public void coalTest() {
		assertEquals("El n�mero de elementos no es el correcto", cola.size(), 0);
	}

	@Test
	public void testAdd() {
		cola.enqueue("hola");
		assertEquals("El n�mero de elementos no es el correcto", cola.size(), 1);
		setUpEscenario2();
		cola.enqueue("como");
		assertEquals("El n�mero de elementos no es el correcto", cola.size(), 2);
		setUpEscenario3();
		cola.enqueue("Bien");
		assertEquals("El n�mero de elementos no es el correcto", cola.size(), 4);
	}

	@Test
	public void testPop() {
		cola.dequeue();
		assertEquals("El n�mero de elementos no es el correcto", cola.size(), 0);
		setUpEscenario2();
		cola.dequeue();
		assertEquals("El n�mero de elementos no es el correcto", cola.size(), 0);
		setUpEscenario3();
		cola.dequeue();
		assertEquals("El n�mero de elementos no es el correcto", cola.size(), 2);
		assertEquals("El  elemento no es el correcto", cola.first(), "como");
	}

	@Test
	public void testIsEmpy() {
		assertTrue("deberia ser vacio ", cola.isEmpty());
		setUpEscenario2();
		assertFalse("No deberia ser vacio ", cola.isEmpty());
	}

	@Test
	public void testSize() {
		assertEquals("Deberia encontrar el primer elemento", 0, cola.size());
		setUpEscenario2();
		assertEquals("Deberia encontrar el primer elemento", 1, cola.size());
		setUpEscenario3();
		assertEquals("Deberia encontrar el primer elemento", 3, cola.size());

	}

}
