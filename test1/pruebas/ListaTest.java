package pruebas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Lista;

public class ListaTest {
	private Lista<String> lista;

	@Before
	public void setUpEscenario1() {
		lista = new Lista<>();
	}

	public void setUpEscenario2() {
		lista = new Lista<>();
		lista.add("hola");
	}

	public void setUpEscenario3() {
		lista = new Lista<>();
		lista.add("hola");
		lista.add("como");
		lista.add("estas");
	}

	@Test
	public void ListaTest() {
		assertEquals("El n�mero de elementos no es el correcto", lista.size(), 0);
	}

	@Test
	public void testAdd() {
		lista.add("hola");
		assertEquals("El n�mero de elementos no es el correcto", lista.size(), 1);
		setUpEscenario2();
		lista.add("como");
		assertEquals("El n�mero de elementos no es el correcto", lista.size(), 2);
		setUpEscenario3();
		lista.add("--Bien");
		assertEquals("El n�mero de elementos no es el correcto", lista.size(), 4);
	}

	@Test
	public void testDelete() {
		lista.delete("hola");
		assertEquals("El n�mero de elementos no es el correcto", lista.size(), 0);
		setUpEscenario2();
		lista.delete("hola");
		assertEquals("El n�mero de elementos no es el correcto", lista.size(), 0);
		setUpEscenario3();
		lista.delete("como");
		assertEquals("El n�mero de elementos no es el correcto", lista.size(), 2);
	}

	@Test
	public void testBuscar() {
		assertFalse("No deberia encontrar nada", lista.buscar("hola"));
		setUpEscenario2();
		assertTrue("deberia encontrar el elemento solicitado", lista.buscar("hola"));
		setUpEscenario3();
		assertTrue("Deberia encontrar el elemento en el medio de la lista", lista.buscar("como"));
	}

	@Test
	public void testGetInt() {
		assertNull("No deberia encontrar nada", lista.get(0));
		setUpEscenario2();
		assertEquals("Deberia encontrar el primer elemento", "hola", lista.get(0));
		setUpEscenario3();
		assertEquals("Deberia encontrar el primer elemento", lista.get(0), "estas");
		assertEquals("Deberia encontrar el segundo elemento", lista.get(1), "como");
		assertEquals("Deberia encontrar el ultimo elemento", lista.get(2), "hola");
	}

	@Test
	public void testGetObj() {
		assertNull("No deberia encontrar nada", lista.get("hola"));
		setUpEscenario2();
		assertEquals("Deberia encontrar el primer elemento", lista.get("hola"), "hola");
		setUpEscenario3();
		assertEquals("Deberia encontrar el primer elemento", lista.get("hola"), "hola");
		assertEquals("Deberia encontrar el segundo elemento", lista.get("como"), "como");
		assertEquals("Deberia encontrar el tercer elemento", lista.get("estas"), "estas");
	}

	@Test
	public void testGetCurretn() {
		assertNull("No deberia encontrar nada", lista.getCurrent());
		setUpEscenario2();
		assertEquals("Deberia encontrar el primer elemento", "hola", lista.getCurrent());
		setUpEscenario3();
		assertEquals("Deberia encontrar el primer elemento", "estas", lista.getCurrent());

	}

	@Test
	public void testHasNext() {
		assertFalse("No deberia encontrar nada", lista.hasNext());
		setUpEscenario2();
		assertFalse("No deberia encontrar ", lista.hasNext());
		setUpEscenario3();
		assertTrue("deberia encontrar ", lista.hasNext());
		lista.next();
		assertTrue("deberia encontrar ", lista.hasNext());
		lista.next();
		assertFalse("No deberia encontrar ", lista.hasNext());

	}

	@Test
	public void testListing() {
		setUpEscenario2();
		lista.listing();
		assertEquals("Deberia encontrar el primer elemento", lista.get(0), lista.getCurrent());
		setUpEscenario3();
		assertEquals("Deberia encontrar el primer elemento", lista.get(0), lista.getCurrent());
		lista.next();
		assertNotEquals("no Deberia encontrar el primer elemento", lista.get(0), lista.getCurrent());
		lista.listing();
		assertEquals("Deberia encontrar el primer elemento", lista.get(0), lista.getCurrent());
		lista.next();
		lista.next();
		assertNotEquals("no Deberia encontrar el primer elemento", lista.get(0), lista.getCurrent());
		lista.listing();
		assertEquals("Deberia encontrar el primer elemento", lista.get(0), lista.getCurrent());
	}

	@Test
	public void testNext() {
		setUpEscenario2();
		lista.next();
		assertEquals("Deberia encontrar el primer elemento", null, lista.next());
		setUpEscenario3();

		assertEquals("Deberia encontrar el primer elemento", lista.get(1), lista.next());

		assertEquals("no Deberia encontrar el primer elemento", lista.get(2), lista.next());

		assertEquals("Deberia encontrar el primer elemento", null, lista.next());

	}

	@Test
	public void testSize() {
		assertEquals("Deberia encontrar el primer elemento", 0, lista.size());
		setUpEscenario2();
		assertEquals("Deberia encontrar el primer elemento", 1, lista.size());
		setUpEscenario3();
		assertEquals("Deberia encontrar el primer elemento", 3, lista.size());

	}
}