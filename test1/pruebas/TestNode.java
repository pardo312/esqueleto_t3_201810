package pruebas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import model.data_structures.Node;

public class TestNode {
	private Node nodo;

	@Before
	public void setUpEscenario1() {
		nodo = new Node("1", null);
		System.out.println(nodo.darObjeto());
	}

	@Test
	public void testNode() {
		setUpEscenario1();
		assertNotNull(" deberia ser distinto de null", nodo);
	}

	@Test
	public void testCambiarAnterior() {
		setUpEscenario1();
		Node n1 = new Node("2", null);
		nodo.cambiarAnterior(n1);
		assertEquals("deberia existir anterior", n1.darObjeto(), nodo.darAnterior().darObjeto());
	}

	@Test
	public void testCambiarSiguiente() {
		setUpEscenario1();
		Node n2 = new Node("3", null);
		nodo.cambiarSiguiente(n2);
		assertEquals("deberia existri siguiente", n2, nodo.darSiguiente());
	}

	@Test
	public void testDarObjeto() {
		setUpEscenario1();
		assertEquals("deberia retornar el objeto ", "1", nodo.darObjeto());
	}

	@Test
	public void testDarAnterior() {
		setUpEscenario1();
		assertNull("deberia ser  null", nodo.darAnterior());
		Node n1 = new Node("2", null);
		nodo.cambiarAnterior(n1);
		assertEquals("deberia existri anterior", n1, nodo.darAnterior());
	}

	@Test
	public void testDarSiguiente() {
		setUpEscenario1();
		assertNull("deberia ser  null", nodo.darSiguiente());
		Node n1 = new Node("3", null);
		nodo.cambiarSiguiente(n1);
		assertEquals("deberia existri anterior", n1, nodo.darSiguiente());
	}
}
