package pruebas;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Pila;




public class PilaTest 
{

	private Pila<String> pila1;
	
	
	@Before
    public void setupEscenario1( )
    {
        pila1.push("Hola");
    }
    
    public void setupEscenario2()
    {
    	pila1.pop();
    }
	
	@Test
	public void testPush() 
	{
	
		
		pila1.push("Hi");
		assertTrue( "El dibujo se cre� incorrectamente", pila1.pop().equals("Hi") );
	}
	
	@Test
	public void testPop() {
		
		pila1.pop();
		assertTrue( "El dibujo se cre� incorrectamente", pila1.pop().equals("Hola") );
	}
	
	@Test
	public void testIsEmpty() {
		setupEscenario2( );
		assertNull( "El dibujo se cre� incorrectamente", pila1.pop() );
	}
	
	@Test
	public void testSize() {
	
		assertEquals( "El dibujo se cre� incorrectamente", 1 ,pila1.size() );
	}
	
	@Test
	public void testEnd() {
		
		
		assertTrue( "El dibujo se cre� incorrectamente", pila1.end().equals("Hola") );
	}

}
