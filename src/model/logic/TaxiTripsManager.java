package model.logic;


import java.io.FileNotFoundException;
import java.io.FileReader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.sun.jmx.snmp.Timestamp;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.Cola;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.data_structures.Pila;

public class TaxiTripsManager implements ITaxiTripsManager
{

	// 	
	// Definition of data model 
	private LinkedList<Service> servicios ;
	private LinkedList<Service> serviciosDeArea; 
	private Pila<Service> pilaServ;
	private Lista<Taxi> taxis ;
	private Cola<Service> colaDeSer;
	private Pila<Service> pila1;

	public void loadServices (String serviceFile) 
	{		
		pila1  = new Pila<Service>();
		servicios = new Lista<Service>();
		String compani = "NaN";
		String dropoff_census_tract = "NaN";
		String dropoff_centroid_latitude  = "NaN";
		String dropoff_centroid_longitude = "NaN";
		JsonObject dropoff_localization_obj = null; 
		Lista<Double> dropoff_localization = new Lista<Double>();
		String dropoff_community_area = "NaN";
		String extras = "NaN";
		String fare = "NaN";
		String payment_type = "NaN";
		String pickup_census_tract = "NaN";
		String pickup_centroid_latitude  = "NaN";
		String pickup_centroid_longitude = "NaN";
		JsonObject pickup_localization_obj = null; 
		Lista<Double> pickup_localization = new Lista<Double>();
		String pickup_community_area = "NaN";
		String taxi_id = "NaN";
		String tips = "NaN";
		String tolls = "NaN";
		String trip_end_timestamp = "NaN";
		String trip_id = "NaN";
		String trip_miles = "NaN";
		String trip_seconds = "NaN";
		String trip_start_timestamp = "NaN";
		String trip_total = "NaN";

		JsonParser parser = new JsonParser();

		try {


			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{

				JsonObject obj = (JsonObject) arr.get(i);

				if ( obj.get("company") != null ) {
					compani = obj.get("company").getAsString();}				
				//--------------------------------

				if ( obj.get("dropoff_census_tract") != null ) {
					dropoff_census_tract  = obj.get("dropoff_census_tract").getAsString();
				}

				if ( obj.get("dropoff_centroid_latitude") != null ) {
					dropoff_centroid_latitude  = obj.get("dropoff_centroid_latitude").getAsString();
				}			

				if ( obj.get("dropoff_centroid_longitude") != null )
				{
					dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); 
				}

				if ( obj.get("dropoff_centroid_location") != null )
				{
					dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();
					JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();				
					double longitude = dropoff_localization_arr.get(0).getAsDouble();
					double latitude = dropoff_localization_arr.get(1).getAsDouble();
					dropoff_localization.add(longitude);
					dropoff_localization.add(latitude);			
				}
				else
				{
					dropoff_localization.add(null);
					dropoff_localization.add(null);	
				}
				if ( obj.get("dropoff_community_area") != null ) {
					dropoff_community_area = obj.get("dropoff_community_area").getAsString();
				}

				//--------------------------------


				if ( obj.get("extras") != null ) {
					extras = obj.get("extras").getAsString();
				}
				if ( obj.get("fare") != null ) {
					fare = obj.get("fare").getAsString();
				}
				if ( obj.get("payment_type") != null ) {
					payment_type = obj.get("payment_type").getAsString();
				}

				//--------------------------------

				if ( obj.get("pickup_census_tract") != null ) {
					pickup_census_tract  = obj.get("pickup_census_tract").getAsString();
				}

				if ( obj.get("pickup_entroid_latitude") != null ) {
					pickup_centroid_latitude  = obj.get("pickup_centroid_latitude").getAsString();
				}			

				if ( obj.get("pickup_centroid_longitude") != null ){
					pickup_centroid_longitude = obj.get("pickup_centroid_longitude").getAsString(); 
				}

				if ( obj.get("pickup_centroid_location") != null )
				{ 
					pickup_localization_obj =obj.get("pickup_centroid_location").getAsJsonObject();
					JsonArray pickup_localization_arr = pickup_localization_obj.get("coordinates").getAsJsonArray();				
					double longitud= pickup_localization_arr.get(0).getAsDouble();
					double latitud = pickup_localization_arr.get(1).getAsDouble();
					pickup_localization.add(longitud);
					pickup_localization.add(latitud);			
				}
				else
				{
					pickup_localization.add(null);
					pickup_localization.add(null);	
				}
				if ( obj.get("pickup_community_area") != null ){
					pickup_community_area = obj.get("pickup_community_area").getAsString();
				}

				//----------
				if ( obj.get("taxi_id") != null ){
					taxi_id = obj.get("taxi_id").getAsString();
				}
				if ( obj.get("tips") != null ){
					tips= obj.get("tips").getAsString();
				}
				if ( obj.get("tolls") != null ){
					tolls = obj.get("tolls").getAsString();
				}
				if ( obj.get("trip_end_timestamp") != null ){
					trip_end_timestamp = obj.get("trip_end_timestamp").getAsString();
				}
				if ( obj.get("trip_id") != null ){
					trip_id = obj.get("trip_id").getAsString();
				}
				if ( obj.get("trip_miles") != null ){
					trip_miles = obj.get("trip_miles").getAsString();
				}
				if ( obj.get("trip_seconds") != null ){
					trip_seconds = obj.get("trip_seconds").getAsString();
				}
				if ( obj.get("trip_start_timestamp") != null ){
					trip_start_timestamp = obj.get("trip_start_timestamp").getAsString();
				}
				if ( obj.get("trip_total") != null ){
					trip_total = obj.get("trip_total").getAsString();
				}
				Service servicio = new Service(compani, dropoff_census_tract, dropoff_centroid_latitude, dropoff_localization, dropoff_centroid_longitude, dropoff_community_area, extras, fare, payment_type, pickup_census_tract, pickup_centroid_latitude, pickup_localization, pickup_centroid_longitude, pickup_community_area, taxi_id, tips, tolls, trip_end_timestamp, trip_id, trip_miles, trip_seconds, trip_start_timestamp, trip_total);
				servicios.add(servicio);



			}

		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	}

	public void loadServices(String serviceFile, String taxiId) 
	{
		// TODO Auto-generated method stub
		loadServices(serviceFile);
		pilaServ = loadServicesPila(taxiId);
		colaDeSer = loadServicesCola(taxiId);
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);

	}


	public Cola<Service> loadServicesCola(String ptaxiId)		
	{
		colaDeSer = new Cola<Service>();
		servicios.listing();
		while(servicios.hasNext())
		{		
			Service ser = (Service) servicios.getCurrent();			
			if(ser.getTaxiId().equals(ptaxiId))
			{				
				colaDeSer.enqueue((ser));				
			}
			servicios.next();
		}
		return colaDeSer;
	}




	public Pila<Service> loadServicesPila(String ptaxiId)		
	{
		pila1 = new Pila<Service>();
		servicios.listing();
		while(servicios.hasNext())
		{

			Service ser = (Service) servicios.getCurrent();
			if(ser.getTaxiId().equals(ptaxiId))
			{				
					pila1.push(ser);			
			}
			servicios.next();
		}
		return pila1;
	}
	
	@Override
	public int[] servicesInInverseOrder()
	{			
		int contOrden = 0;
		int contNoOrden = 0;		
		int [] resultado = new int[2];
		Pila<Service> a = pila1;
		System.out.println(a.size());
		if (!a.isEmpty())
		{
			Service ser1 = (Service) a.pop();

			while (!a.isEmpty())
			{
				Service ser2 = (Service) a.pop();
				if( ser1.geTimeIni().after(ser2.geTimeIni()))
				{
					contOrden++;
				}
				else
				{
					contNoOrden++;
				}
				ser1 = ser2;
			}
			contOrden++;
		}
		resultado[0]= contOrden;
		resultado[1]= contNoOrden;
		return resultado;
	}



	@Override
	public int [] servicesInOrder()
	{
		// TODO Auto-generated method stub
		int contOrden = 0;
		int contNoOrden = 0;		
		int [] resultado = new int[2];
		Cola<Service> a = colaDeSer;
		System.out.println(a.size());
		if (!a.isEmpty())
		{
			Service ser1 = (Service) a.dequeue();

			while (!a.isEmpty())
			{
				Service ser2 = (Service) a.dequeue();
				if( ser1.geTimeIni().after(ser2.geTimeIni() ))
				{
					contNoOrden++;
				}
				else
				{
					contOrden++;
				}
				ser1 = ser2;
			}
			contOrden++;
		}
		resultado[0]= contOrden;
		resultado[1]= contNoOrden;
		return resultado;
	}

}



