package model.vo;



import java.sql.Timestamp;

import model.data_structures.LinkedList;
import model.data_structures.Lista;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {


	private Taxi tax;
	private String 	dropoff_census_tract;
	private String 	dropoff_centroid_latitude;
	private Lista<Double>	dropoff_centroid_location;
	private String	dropoff_centroid_longitude;
	private String 	dropoff_community_area;
	private String 	extras;
	private String 	fare;
	private String 	payment_type;
	private String	pickup_census_tract;
	private String 	pickup_centroid_latitude;
	private Lista<Double>	pickup_centroid_location;
	private String  pickup_centroid_longitude;
	private String	pickup_community_area;

	private String 	tips;
	private String  tolls;
	private String  trip_end_timestamp;
	private String  trip_id;
	private String trip_miles;
	private String  trip_seconds;
	private String	trip_start_timestamp;
	private String 	trip_total;
	private String  Trip_Id;


	public Service(String pcompany,  String pdropoff_census_tract, String pdropoff_centroid_latitude,
			Lista<Double> pdropoff_centroid_location, String pdropoff_centroid_longitude, String pdropoff_community_area,
			String pextras, String pfare, String pPayment_type, String pPickup_census_tract,
			String pPickup_centroid_latitude, Lista<Double> pPickup_centroid_location, String pPickup_centroid_longitude,
			String pPickup_community_area, String ptaxi_id, String ptips, String ptolls, String ptrip_end_timestamp,
			String ptrip_id, String ptrip_miles, String ptrip_seconds, String ptrip_start_timestamp, String ptrip_total) {


		dropoff_census_tract = pdropoff_census_tract;
		dropoff_centroid_latitude = pdropoff_centroid_latitude;
		dropoff_centroid_location = pdropoff_centroid_location;
		dropoff_centroid_longitude = pdropoff_centroid_longitude;
		dropoff_community_area = pdropoff_community_area;
		extras = pextras;
		fare = pfare;
		payment_type = pPayment_type;
		pickup_census_tract = pPickup_census_tract;
		pickup_centroid_latitude = pPickup_centroid_latitude;
		pickup_centroid_location = pPickup_centroid_location;
		pickup_centroid_longitude = pPickup_centroid_longitude;
		pickup_community_area = pPickup_community_area;
		tips = ptips;
		tolls = ptolls;
		trip_end_timestamp = ptrip_end_timestamp;
		trip_id = ptrip_id;
		trip_miles = ptrip_miles;
		trip_seconds = ptrip_seconds;
		trip_start_timestamp = ptrip_start_timestamp;
		trip_total = ptrip_total;
		tax = new Taxi (pcompany , ptaxi_id);
	}


	/**
	 * @return id - Trip_id
	 */
	public String getTripId() 
	{
		// TODO Auto-generated method stub

		return trip_id;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		return tax.getTaxiId();
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		int i = Integer.parseInt(trip_seconds);
		return i;
	}



	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		double d = Double.parseDouble(trip_miles);
		return d;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		double d = Double.parseDouble(trip_total);
		return d;
	}

	public Taxi getTaxi() {
		return tax;
	}

	public int getArea() 
	{
		int i = Integer.parseInt(dropoff_community_area);
		return i;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Timestamp geTimeIni() 
	{

		String[] fecha = trip_start_timestamp.split("T");
		String[] AMD = fecha[0].split("-");
		int a�o = Integer.parseInt( AMD[0]);
		int mes = Integer.parseInt( AMD[1]);
		int dia = Integer.parseInt( AMD[2]);
		String[] tiempo = fecha[1].split(":");		
		int hh = Integer.parseInt( tiempo[0]);
		int mm = Integer.parseInt( tiempo[1]);
		String s=tiempo[2].substring(0, 2);
		int seg = Integer.parseInt(s);
		String n=tiempo[2].substring(4);
		int nano = Integer.parseInt( n);
		Timestamp time = new Timestamp(a�o,mes,dia,hh,mm,seg,nano);
		return time;
	}

	public Timestamp geTimeFin() 
	{
		String[] fecha = trip_end_timestamp.split("T");
		String[] AMD = fecha[0].split("-");
		int a�o = Integer.parseInt( AMD[0]);
		int mes = Integer.parseInt( AMD[1]);
		int dia = Integer.parseInt( AMD[2]);
		String[] tiempo = fecha[1].split(":");		
		int hh = Integer.parseInt( tiempo[0]);
		int mm = Integer.parseInt( tiempo[1]);
		String s=tiempo[2].substring(0, 2);
		int seg = Integer.parseInt(s);
		String n=tiempo[2].substring(4);
		int nano = Integer.parseInt( n);
		Timestamp time = new Timestamp(a�o,mes,dia,hh,mm,seg,nano);
		return time;
	}
}
