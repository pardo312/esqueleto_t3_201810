package model.vo;

import model.data_structures.Cola;
import model.data_structures.Lista;
import model.data_structures.Pila;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	
	private String  compania;
	private String taxiId;
	
	
	public Taxi (String pCompania, String pTaxId)
	{
		compania = pCompania;
		taxiId = pTaxId;
		
	}
	
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return compania;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return 0;
	}	
}
