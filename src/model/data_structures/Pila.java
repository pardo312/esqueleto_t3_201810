package model.data_structures;

public class Pila <T extends Comparable<T>>
{


	private Node nodo;
	private T elem;
	private Node Next;
	private Node end;
	private int size;

	public Pila() 
	{
		end = null;
		size = 0;
	}

	public void push(T pElemento) 
	{
		Node new_node = new Node(pElemento, null);
		if (end == null)
			end = new_node;
		else {
			new_node.cambiarSiguiente(end);
			end = new_node;
		}
		size++;
	}


	public T pop() 
	{
		if (end == null)
		{
			return null;
		}
		T elemento = (T) end.darObjeto();
		end = end.darSiguiente();
		size--;
		return elemento;
	}

	public boolean isEmpty() 
	{

		return (size == 0);
	}

	public int size() 
	{
		return size;
	}

	public T end() 
	{

		if (end == null)
			return null;
		else
			return (T) end.darObjeto();
	}


}
