package model.data_structures;

public class Node <T extends Comparable<T>> 
{
	private T objeto;
	
	private Node siguiente;
	
	private Node anterior;
	

	
	public Node (T pObjeto, Node pAnterior)
	{
		objeto = pObjeto;		
		anterior = pAnterior;
	}
	
	public Node darSiguiente( )
	{
		return siguiente;
	}
	
	public Node darAnterior( )
	{
		return anterior;
	}
	
	public void cambiarSiguiente(Node pSiguiente)
	{
		siguiente = pSiguiente;
	}
	
	public void cambiarAnterior (Node pAnterior)
	{
		anterior = pAnterior;
	}	
	
	public T darObjeto()
	{
		return objeto;
	}


	
}
