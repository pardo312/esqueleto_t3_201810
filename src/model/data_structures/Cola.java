package model.data_structures;

public class Cola< T extends Comparable<T>> implements IQueue<T>
{
	
	private Node first;
	private Node end;
	private int size;

	public Cola() 
	{
		end = null;
		size = 0;
	}

	public void enqueue(T o) {
		Node new_node = new Node(o, null);
		if (first == null) {
			first = new_node;
			end = new_node;
			System.out.println("ingreso el primero a la cola");
		}
		else 
		{
			end.cambiarAnterior(new_node);
			end = new_node;
			System.out.println("ingreso a la cola");
		}
		size++;
	}; 

	public	T dequeue() {
		if (first == null)
		{
			return null;
		}		
		T o = (T) first.darObjeto();
		first = first.darAnterior();
		size--;
		return o;
	} 

	public boolean isEmpty() {
		return (size == 0);
	}

	public int size() {
		return size;
	}

	public Object first() {
		if (first == null)
			return null;
		else
			return first.darObjeto();
	}

}

